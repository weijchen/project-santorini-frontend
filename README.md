# Santorini
- Reproduction of a popular multiplayer board
- Shipped TypeScript code to create the user interface for Santorini with React and hosted the application with Node and Express
- Built the backend services with Java, managing API requests and game states using NanoHTTPD

## Screenshots
![Game-1](./game-1.png)
![Game-2](./game-2.png)
![Game-3](./game-3.png)
![Game-4](./game-4.png)

## Appendix 1: Santorini Rules

Santorini has very simple rules, but the game is very extensible. You can find the original rules [online](https://roxley.com/products/santorini). Beyond the actual board game, you can also find an App that implements the game if you want to try to play it.

In a nutshell, the rules are as follows: The game is played on a 5 by 5 grid, where each grid can contain towers consisting of blocks and domes. Two players have two workers each on any field of the grid. Throughout the game, the workers move around and build towers. The first worker to make it on top of a level-3 tower wins. Note that though the official rules require that if a player cannot further move any worker, she/he will lose, you don't need to consider this as a winning condition in this homework. You also don’t need to handle more than two players.
