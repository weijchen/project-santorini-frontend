import React from 'react';
import { Grid } from './game';

interface Props {
  grid: Grid
  winnerID: number
  onClick?: () => void;
}

class BoardGrid extends React.Component<Props> {
  render(): React.ReactNode {
    const playable = !this.props.grid.occupied ? 'playable' : '';
    const hasWinner = this.props.winnerID !== -1;
    return (

      <div>
        {hasWinner ? (<div className={`grid ${playable}`}>
          <p>{"Player: " + this.props.grid.playerID + " | Worker: " + this.props.grid.workerID}</p><p>{"LV: " + this.props.grid.level}</p>
          <div className={`lv${this.props.grid.level}`} ></div>
        </div>) : (<div className={`grid ${playable}`} onClick={this.props.onClick}>
          <p>{"Player: " + this.props.grid.playerID + " | Worker: " + this.props.grid.workerID}</p><p>{"LV: " + this.props.grid.level}</p>
          <div className={`lv${this.props.grid.level}`} ></div>
        </div>)}
      </div>
    )
  }
}

export default BoardGrid;
