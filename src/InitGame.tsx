import React from 'react';
import { IInitGame, Grid } from './game';
import BoardGrid from './Grid';
import { GAME_STAGE, GOD_TYPE_INT, GOD_TYPE_STR } from './constant'

interface Props {
  grids: Grid[];
  handleGodSelect: (p1: String, p2: String) => void
  handleGridChange: (grids: Grid[]) => void
  handleWorkerInit: () => void
  updateCurPlayer: (newPlayerID: number) => void
}

class InitGame extends React.Component<Props, IInitGame> {
  private hasSelectGod: boolean = false;

  constructor(props: Props) {
    super(props)

    this.state = { currentPlayer: 0, currentWorker: 0, selectedGod: -1 };
  }

  createGrid = (grid: Grid, index: number): React.ReactNode => {
    if (!grid.occupied)
      return (
        <div key={index}>
          <BoardGrid grid={grid} onClick={() => this.initializeWorker(grid.row, grid.col)} winnerID={-1}></BoardGrid>
        </div>
      )
    else
      return (
        <div key={index}><BoardGrid grid={grid} winnerID={-1}></BoardGrid></div>
      )
  }

  initializeWorker = async (row: number, col: number) => {
    if (this.state.currentPlayer >= 2) {
      return;
    }
    const currentPlayer = this.state.currentPlayer;
    const currentWorker = this.state.currentWorker;

    const response = await fetch(`/initializeWorker?playerID=${currentPlayer}&workerID=${currentWorker}&row=${row}&col=${col}`);
    const json = await response.json();
    this.props.handleGridChange(json['grids']);

    this.setState({ currentWorker: this.state.currentWorker + 1 });

    if (this.state.currentWorker === 1) {
      this.setState({ currentPlayer: this.state.currentPlayer + 1, currentWorker: 0 })
      this.props.updateCurPlayer(this.state.currentPlayer + 1)
    }
  }

  selectGodCard = async (index: number) => {
    const prevSelect = this.state.selectedGod;
    if (prevSelect !== -1) {
      const response = await fetch(`/initializePlayer?p1=${prevSelect}&p2=${index}`);
      const json = await response.json();
      this.hasSelectGod = true;
      this.setState({ currentPlayer: 0 });
      this.props.handleGodSelect(json['p1'], json['p2'])
    } else {
      this.setState({ currentPlayer: 1, selectedGod: index });
    }
  }

  render(): React.ReactNode {
    return (
      <>
        <div className='status'>
          {this.state.currentPlayer < 2 ?
            <p>Current player: {this.state.currentPlayer}</p> : ''}
        </div>
        {!this.hasSelectGod ?
          <div>
            <p>Select your God Power</p>
            <div style={{ display: "inline-flex" }}>
              <button className='godCard' onClick={() => this.selectGodCard(GOD_TYPE_INT.DEMETER)} disabled={this.state.selectedGod === GOD_TYPE_INT.DEMETER}>{GOD_TYPE_STR.DEMETER}</button>
              <button className='godCard' onClick={() => this.selectGodCard(GOD_TYPE_INT.MINOTAUR)} disabled={this.state.selectedGod === GOD_TYPE_INT.MINOTAUR}>{GOD_TYPE_STR.MINOTAUR}</button>
              <button className='godCard' onClick={() => this.selectGodCard(GOD_TYPE_INT.PAN)} disabled={this.state.selectedGod === GOD_TYPE_INT.PAN}>{GOD_TYPE_STR.PAN}</button>
              <button className='godCard' onClick={() => this.selectGodCard(GOD_TYPE_INT.MORTAL)} disabled={this.state.selectedGod === GOD_TYPE_INT.MORTAL}>{GOD_TYPE_STR.MORTAL}</button>
              <button className='godCard' onClick={() => this.selectGodCard(GOD_TYPE_INT.ARTEMIS)} disabled={this.state.selectedGod === GOD_TYPE_INT.ARTEMIS}>{GOD_TYPE_STR.ARTEMIS}</button>
              <button className='godCard' onClick={() => this.selectGodCard(GOD_TYPE_INT.HEPHAESTUS)} disabled={this.state.selectedGod === GOD_TYPE_INT.HEPHAESTUS}>{GOD_TYPE_STR.HEPHAESTUS}</button>
              <button className='godCard' onClick={() => this.selectGodCard(GOD_TYPE_INT.APOLLO)} disabled={this.state.selectedGod === GOD_TYPE_INT.APOLLO}>{GOD_TYPE_STR.APOLLO}</button>
            </div>
          </div> :
          <div>
            {this.state.currentPlayer < 2 ? <p>Initialize your workers</p> : <p><button onClick={() => { this.props.updateCurPlayer(0); this.props.handleWorkerInit() }}>Start the game!</button></p>}
            <div className='wrapper'>
              <div id="board" >
                {this.props.grids.map((grid: Grid, i: number) => this.createGrid(grid, i))}
              </div>
            </div>
          </div>}

      </>
    )
  }
}

export default InitGame;
