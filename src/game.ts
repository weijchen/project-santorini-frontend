interface GameState {
  grids: Grid[];
  curPlayerID: number;
  curWorkerID: number;
  hasInitGame: boolean;
  playerOneType: String;
  playerTwoType: String;
  curRow: number;
  curCol: number;
  curStage: number;
  candidates: Grid[];
  winnerID: number;
  godSkillActivated: boolean;
}

interface IInitGame {
  currentPlayer: number;
  currentWorker: number;
  selectedGod: number;
}

interface Grid {
  row: number;
  col: number;
  occupied: boolean;
  level: number;
  playerID: number;
  workerID: number;
}

export type { GameState, Grid, IInitGame };
