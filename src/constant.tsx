
export const GAME_STAGE = {
  MOVE: 0,
  BUILD: 1
}

export const GOD_TYPE_INT = {
  DEMETER: 0,
  MINOTAUR: 1,
  PAN: 2,
  MORTAL: 3,
  ARTEMIS: 4,
  HEPHAESTUS: 5,
  APOLLO: 6,
}

export const GOD_TYPE_STR = {
  DEMETER: "Demeter",
  MINOTAUR: "Minotaur",
  PAN: "Pan",
  MORTAL: "Mortal",
  ARTEMIS: "Artemis",
  HEPHAESTUS: "Hephaestus",
  APOLLO: "Apollo",
}

export const GOD_TYPE_SKILL = [
  { id: 0, type: "Demeter", value: "Can build twice, build on the same grid will cancel the skill" },
  { id: 1, type: "Minotaur", value: "Click name to enable Charge" },
  { id: 2, type: "Pan", value: "Also win the game by moving down two levels" },
  { id: 3, type: "Mortal", value: "Nothing people are nothing special" },
  { id: 4, type: "Artemis", value: "Can move twice, move back will cancel the skill" },
  { id: 5, type: "Hephaestus", value: "Can build on the same grid twice (but not dome)" },
  { id: 6, type: "Apollo", value: "Can swap position with other player's workers" },
];
