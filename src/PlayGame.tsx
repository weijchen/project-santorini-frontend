import React from 'react';
import { GameState, Grid } from './game';
import BoardGrid from './Grid';
import { GAME_STAGE, GOD_TYPE_INT, GOD_TYPE_STR } from './constant'

interface Props {
  grids: Grid[];
  candidates: Grid[];
  curPlayerID: number;
  curStage: number;
  winnerID: number;
  playerOneType: String;
  playerTwoType: String;
  godSkillActivated: boolean;
  handleGetMove: (grid: Grid, workerID: number) => void;
  handleMove: (grid: Grid) => void;
  handleGetCharge: (grid: Grid) => void;
  handleGetSwap: (grid: Grid) => void;
  handleGetBuild: () => void;
  handleBuild: (grid: Grid) => void;
  handleTakeTurn: () => void;
  updateCurStage: (nextStage: number) => void;
  closeGodSkill: () => void;
  updateCurWorker: (newWorkerID: number) => void;
}

class PlayGame extends React.Component<Props, GameState> {
  private stopHighlightMove: boolean = false;
  private stopHighlightBuild: boolean = false;
  private hasFinishSelectWorker: boolean = false;
  private hasFinishMove: boolean = false;
  private hasFinishSelectBuild: boolean = false;
  private hasFinishBuild: boolean = false;
  private curPlayerType: String = '';
  private hasMoveAgain: boolean = false;
  private prevWorkerID: number = -1;
  private hasBuiltAgain: boolean = false;


  highlight = (row: number, col: number): boolean => {
    for (const candidate of this.props.candidates) {
      if (candidate.row === row && candidate.col === col) {

        return true;
      }
    }
    return false;
  }

  handleSelectWorker = (grid: Grid) => {
    if (grid.workerID === -1 || this.props.curPlayerID !== grid.playerID) return;
    this.hasFinishSelectWorker = true;
    this.props.updateCurWorker(grid.workerID);
  }

  handleGetMove = (grid: Grid, workerID: number) => {
    if (!this.hasFinishSelectWorker || this.props.curPlayerID !== grid.playerID) return;

    // Minotaur
    if (this.props.godSkillActivated && ((this.props.curPlayerID === 0 && this.props.playerOneType === GOD_TYPE_STR.MINOTAUR) || (this.props.curPlayerID === 1 && this.props.playerTwoType === GOD_TYPE_STR.MINOTAUR))) {
      this.props.handleGetCharge(grid);
      return;
    }
    if (this.props.godSkillActivated && ((this.props.curPlayerID === 0 && this.props.playerOneType === GOD_TYPE_STR.APOLLO) || (this.props.curPlayerID === 1 && this.props.playerTwoType === GOD_TYPE_STR.APOLLO))) {
      this.props.handleGetSwap(grid);
      return;
    }
    this.prevWorkerID = workerID;
    this.props.handleGetMove(grid, workerID);
  }

  handleMoveWorker = (grid: Grid) => {
    this.curPlayerType = this.getCurrentPlayerType();
    this.hasFinishMove = true;
    this.props.updateCurStage(GAME_STAGE.BUILD);
    this.stopHighlightMove = true;
    this.stopHighlightBuild = true;

    // Artemis
    if (this.curPlayerType === GOD_TYPE_STR.ARTEMIS && !this.hasMoveAgain) {
      this.hasFinishMove = false;
      this.props.updateCurStage(GAME_STAGE.MOVE)
      this.hasMoveAgain = true;
      this.stopHighlightMove = false;
      this.props.handleGetMove(grid, this.prevWorkerID);
    }

    if (this.curPlayerType === GOD_TYPE_STR.ARTEMIS && this.hasMoveAgain) {
      this.props.handleGetMove(grid, this.prevWorkerID);
    }

    this.props.handleMove(grid);
  }

  handleBuildTower = (grid: Grid) => {
    this.hasMoveAgain = false;
    this.curPlayerType = this.getCurrentPlayerType();
    this.hasFinishBuild = true;
    this.props.updateCurStage(GAME_STAGE.MOVE);

    // Demeter
    if (this.curPlayerType === GOD_TYPE_STR.DEMETER && !this.hasBuiltAgain) {
      this.hasFinishBuild = false;
      this.props.updateCurStage(GAME_STAGE.BUILD);
      this.hasBuiltAgain = true;
    }

    // Hephaestus
    if (this.curPlayerType === GOD_TYPE_STR.HEPHAESTUS && !this.hasBuiltAgain) {
      this.hasFinishBuild = false;
      this.props.updateCurStage(GAME_STAGE.BUILD);
      this.hasBuiltAgain = true;
    }

    this.props.handleBuild(grid);
  }

  getCurrentPlayerType = (): String => {
    return this.props.curPlayerID === 0 ? this.props.playerOneType : this.props.playerTwoType;
  }

  initState = () => {
    this.hasFinishSelectWorker = false;
    this.hasFinishMove = false;
    this.hasFinishSelectBuild = false;
    this.hasFinishBuild = false;
    this.stopHighlightMove = false;
    this.stopHighlightBuild = false;

    // Demeter and Hephaestus
    this.hasBuiltAgain = false;
  }

  createGrid = (grid: Grid, index: number): React.ReactNode => {
    const highlightMove = !this.stopHighlightMove && this.highlight(grid.row, grid.col) ? 'highlightMove' : '';
    const highlightBuild = !this.stopHighlightBuild && this.highlight(grid.row, grid.col) ? 'highlightBuild' : '';
    const curStage = this.props.curStage;
    return (
      <div>
        {curStage === GAME_STAGE.MOVE ? (<div key={index} className={`${highlightMove}`}>
          {highlightMove ? (<BoardGrid grid={grid} onClick={() => this.handleMoveWorker(grid)} winnerID={this.props.winnerID}></BoardGrid>) : (<BoardGrid grid={grid} onClick={() => { this.handleSelectWorker(grid); this.handleGetMove(grid, grid.workerID); }} winnerID={this.props.winnerID}></BoardGrid>)}
        </div>) : (<div key={index} className={`${highlightBuild}`}>
          {highlightBuild ? (<BoardGrid grid={grid} onClick={() => this.handleBuildTower(grid)} winnerID={this.props.winnerID}></BoardGrid>) : (<BoardGrid grid={grid} onClick={() => { this.handleSelectWorker(grid); this.handleGetMove(grid, grid.workerID); }} winnerID={this.props.winnerID}></BoardGrid>)}
        </div>)}

      </div>
    )
  }

  renderInstruction() {
    if (!this.hasFinishSelectWorker) {
      return <p>Pick one of your worker</p>
    }
    if (!this.hasFinishMove) {
      return <p>Move your worker</p>
    }
    if (!this.hasFinishSelectBuild) {
      return <button className='progress-btn' onClick={() => { this.hasFinishSelectBuild = true; this.stopHighlightBuild = false; this.props.updateCurStage(1); this.props.handleGetBuild(); }}>Confirm move</button>
    }
    // Demeter and Hephaestus
    if (!this.hasFinishBuild) {
      return this.hasBuiltAgain ? this.curPlayerType === GOD_TYPE_STR.DEMETER ? (<p>Pick another grid to build (build on the same grid will pass)</p>) : (<p>Pick the same grid to build (build on other grids will pass)</p>) : <p>Pick a grid to build</p>
    }
    return <button className='progress-btn' onClick={() => { this.initState(); this.props.handleTakeTurn(); }}>Take turn</button>
  }

  render(): React.ReactNode {
    return (
      <>
        <div>
          <div className='status'>
            {this.props.winnerID === -1 ? (
              <div><p>Current player: {this.props.curPlayerID}</p><div className='substatus'>
                {this.renderInstruction()}
              </div></div>) : (<div><p>Winner of the game: Player {this.props.winnerID} !</p></div>)}
            <div>
              {this.props.godSkillActivated ? (
                <p>Skill activated (reselect to see difference)</p>
              ) : (<></>)}</div>
          </div>
          <div className='wrapper'>
            <div id="board" >
              {this.props.grids.map((grid: Grid, i: number) => this.createGrid(grid, i))}
            </div>
          </div>
        </div>
      </>
    )
  }
}

export default PlayGame;
