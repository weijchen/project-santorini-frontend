import React from 'react'
import { GameState, Grid } from './game'
import InitGame from './InitGame'
import './App.css'
import PlayGame from './PlayGame'
import { GAME_STAGE, GOD_TYPE_INT, GOD_TYPE_STR, GOD_TYPE_SKILL } from './constant'

interface Props { }

class App extends React.Component<Props, GameState> {
  private initialized: boolean = false;

  constructor(props: Props) {
    super(props)

    this.state = { grids: [], curPlayerID: 0, curWorkerID: 0, hasInitGame: false, playerOneType: '', playerTwoType: '', curRow: -1, curCol: -1, curStage: 0, candidates: [], winnerID: -1, godSkillActivated: false };
  }

  newGame = async () => {
    const response = await fetch('/newGame');
    const json = await response.json();
    this.setState({ grids: json['grids'] })
  }

  getCurrentPlayer = async () => {
    const response = await fetch('/currentPlayer');
    const json = await response.json();
    this.setState({ curPlayerID: json['curPlayerID'] })
  }

  componentDidMount(): void {
    if (!this.initialized) {
      this.newGame();
      this.getCurrentPlayer();
      this.initialized = true;
    }
  }

  handleGodSelect = (p1: String, p2: String) => {
    this.setState({ playerOneType: p1, playerTwoType: p2 });
  }

  handleWorkerInit = () => {
    this.setState({ hasInitGame: true })
  }

  handleGridChange = (grids: Grid[]) => {
    this.setState({ grids: grids })
  }

  handleGetMove = async (grid: Grid, workerID: number) => {
    let response;
    let json;
    if (this.state.curStage === GAME_STAGE.MOVE) {
      response = await fetch(`/getMove?workerID=${workerID}`);
      json = await response.json();
      this.setState({ candidates: json["candidates"] })
    }
  }

  handleGetCharge = async (grid: Grid) => {
    let response;
    let json;
    if (this.state.curStage === GAME_STAGE.MOVE) {
      response = await fetch(`/getCharge?workerID=${grid.workerID}`);
      json = await response.json();
      this.setState({ candidates: json["candidates"], curWorkerID: grid.workerID })
    }
  }

  handleGetSwap = async (grid: Grid) => {
    let response;
    let json;
    if (this.state.curStage === GAME_STAGE.MOVE) {
      response = await fetch(`/getSwap?workerID=${grid.workerID}`);
      json = await response.json();
      this.setState({ candidates: json["candidates"], curWorkerID: grid.workerID })
    }
  }

  handleMove = async (grid: Grid) => {
    let response;
    let json;
    response = await fetch(`/move?workerID=${this.state.curWorkerID}&row=${grid.row}&col=${grid.col}`);
    json = await response.json();
    this.setState({ grids: json["grids"] })
    response = await fetch('/checkWin');
    json = await response.json()
    const hasWin = json["hasWin"];
    if (hasWin) {
      this.setState({ winnerID: json["winnerID"] })
    }
    this.closeGodSkill();
  }

  handleGetBuild = async () => {
    let response;
    let json;
    if (this.state.curStage === GAME_STAGE.BUILD) {
      response = await fetch(`/getBuild?workerID=${this.state.curWorkerID}`);
      json = await response.json();
      this.setState({ candidates: json["candidates"] })
    }
  }

  handleBuild = async (grid: Grid) => {
    const response = await fetch(`/build?workerID=${this.state.curWorkerID}&row=${grid.row}&col=${grid.col}`);
    const json = await response.json();
    this.setState({ grids: json["grids"] })
    this.closeGodSkill();
  }

  handleTakeTurn = async () => {
    const response = await fetch('/takeTurn');
    const json = await response.json()
    this.getCurrentPlayer();
    this.updateCurStage(GAME_STAGE.MOVE);
    this.setState({ candidates: [] })
  }

  updateCurStage = (nextStage: number) => {
    this.setState({ curStage: nextStage });
  }

  updateCurWorker = (newWorkerID: number) => {
    this.setState({ curWorkerID: newWorkerID });
  }

  updateCurPlayer = (newPlayerID: number) => {
    this.setState({ curPlayerID: newPlayerID })
  }

  switchGodSkill = (playerType: String, playerID: number) => {
    if (this.state.curPlayerID !== playerID) return;
    if (playerType === GOD_TYPE_STR.MINOTAUR && this.state.curStage === GAME_STAGE.MOVE) {
      this.setState({ godSkillActivated: !this.state.godSkillActivated })
    }
    if (playerType === GOD_TYPE_STR.APOLLO && this.state.curStage === GAME_STAGE.MOVE) {
      this.setState({ godSkillActivated: !this.state.godSkillActivated })
    }
  }

  closeGodSkill = () => {
    this.setState({ godSkillActivated: false })
  }

  render(): React.ReactNode {
    return (
      <>
        <div>
          <div className='main'>
            <div className='player-status'>
              <div className={`player ${this.state.curPlayerID === 0 ? 'activePlayer' : 'p1'} `} onClick={() => this.switchGodSkill(this.state.playerOneType, 0)}>{this.state.playerOneType}</div>
              <div>{GOD_TYPE_SKILL.find(i => i.type === this.state.playerOneType)?.value}
              </div>
            </div>
            <div>
              {this.state.hasInitGame ?
                <PlayGame grids={this.state.grids} curPlayerID={this.state.curPlayerID} handleGetMove={this.handleGetMove} handleGetCharge={this.handleGetCharge} candidates={this.state.candidates} curStage={this.state.curStage} updateCurStage={this.updateCurStage} handleMove={this.handleMove} handleBuild={this.handleBuild} handleGetBuild={this.handleGetBuild} handleTakeTurn={this.handleTakeTurn} winnerID={this.state.winnerID} playerOneType={this.state.playerOneType} playerTwoType={this.state.playerTwoType} closeGodSkill={this.closeGodSkill} godSkillActivated={this.state.godSkillActivated} updateCurWorker={this.updateCurWorker} handleGetSwap={this.handleGetSwap} /> : <InitGame grids={this.state.grids} handleGodSelect={this.handleGodSelect} handleGridChange={this.handleGridChange} handleWorkerInit={this.handleWorkerInit} updateCurPlayer={this.updateCurPlayer} />}
            </div>
            <div className='player-status'>
              <div className={`player ${this.state.curPlayerID === 1 ? 'activePlayer' : 'p2'}`} onClick={() => this.switchGodSkill(this.state.playerTwoType, 1)}>{this.state.playerTwoType}</div>
              <div>{GOD_TYPE_SKILL.find(i => i.type === this.state.playerTwoType)?.value}
              </div>
            </div>
          </div>
          <div id="bottomBar">
            <button onClick={() => { window.location.reload() }}>
              New Game
            </button>
            <button disabled>
              Undo
            </button>
          </div>
        </div>
      </>
    )
  }
}

export default App
